create database if not exists university_db;
use university_db;


CREATE TABLE speciality (
  id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name varchar(45) NOT NULL,
  scholarship_amount varchar(45) NULL
);


CREATE TABLE university_db.group (
  id int NOT NULL,
  name varchar(20) NULL,
  group_code varchar(10) NULL,
  enter_year varchar(5) NULL,
  speciality_id int NOT NULL,

  CONSTRAINT PK_group_id_speciality_id
  PRIMARY KEY (id, speciality_id),
  
  INDEX FK_group_speciality_idx (speciality_id ASC),
  
  CONSTRAINT FK_group_speciality
    FOREIGN KEY (speciality_id)
    REFERENCES speciality (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);


CREATE TABLE student (
  id int NOT NULL,
  first_name varchar(45) NOT NULL,
  last_name varchar(45) NOT NULL,
  middle_name varchar(45) NULL,
  birth_year varchar(45) NULL,
  home_adress varchar(100) NULL,
  photo varchar(45) NULL,
  bio varchar(45) NULL,
  current_rating varchar(45) NULL,
  has_scholarship varchar(5) NULL,
  group_id int NOT NULL,

  CONSTRAINT PK_student_id_group_id
  PRIMARY KEY (id, group_id),
  
  INDEX FK_student_group_idx (group_id ASC),

  CONSTRAINT FK_student_group
    FOREIGN KEY (group_id)
    REFERENCES university_db.group (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);


CREATE TABLE lecturer (
  id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name varchar(45) NULL
);


CREATE TABLE subject (
  id int NOT NULL,
  name varchar(45) NOT NULL,
  is_completed varchar(45) NULL,
  type_of_exam varchar(45) NULL,
  number_of_semester varchar(45) NULL,
  result_module1 varchar(45) NULL,
  result_module2 varchar(45) NULL,
  grade_100 varchar(45) NULL,
  grade_5 varchar(45) NULL,
  lecturer_id int NOT NULL,

  CONSTRAINT PK_subject_id_lecturer_id
  PRIMARY KEY (id, lecturer_id),
  
  INDEX FK_subject_lecturer_idx (lecturer_id ASC),
  
  CONSTRAINT FK_subject_lecturer
    FOREIGN KEY (lecturer_id)
    REFERENCES lecturer (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);


CREATE TABLE student_has_subject (
  student_id int NOT NULL,
  student_group_id int NOT NULL,
  subject_id int NOT NULL,

  CONSTRAINT PK_student_has_subject_student_id_student_group_id_subject_id
  PRIMARY KEY (student_id, student_group_id, subject_id),
  
  INDEX FK_student_has_subject_subject_idx (subject_id ASC),
  INDEX FK_student_has_subject_student_idx (student_id ASC, student_group_id ASC),
  
  CONSTRAINT FK_student_has_subject_student
    FOREIGN KEY (student_id, student_group_id)
    REFERENCES student (id, group_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT FK_student_has_subject_subject
    FOREIGN KEY (subject_id)
    REFERENCES subject (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);


#---lecturer------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ 
insert into lecturer values(1, 'Baraban S.P.');
insert into lecturer values(2, 'Vahanska O.B.');
insert into lecturer values(3, 'Derevan D.F.');
insert into lecturer values(4, 'Sobko M.A.');
insert into lecturer values(5, 'Laba K.F.');

#---subject------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ 
insert into subject values(1, 'Matem Analysis', 'yes', 'exam', '1', '30', '30', '75', '4', 1);
insert into subject values(2, 'Algebra', 'yes', 'exam', '1', '35', '30', '81', '4', 2);
insert into subject values(3, 'Economy', 'yes', 'exam', '1', '40', '35', '75', '4', 3);
insert into subject values(4, 'Filosophy', 'yes', 'zalik', '1', '20', '30', '65', '3', 4);

#---speciality------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ 
insert into speciality values(1, 'Mathematics', '700');
insert into speciality values(2, 'Informatics', '800');
insert into speciality values(3, 'Computer Science', '900');

#---group------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ 
insert into university_db.group values(1, 'PMA-11', '1234', '2013', 1);
insert into university_db.group values(2, 'PKI-11', '4857', '2013', 2);
insert into university_db.group values(3, 'PCS-11', '4534', '2013', 3);

#---student------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ 
insert into student values(1, 'Andriy', 'Shevchenko', 'Petrovych', '1998', 'Lviv, Shevchenko str 4-10', 'link', 'link', '20', 'Yes', 1);
insert into student values(2, 'Sergiy', 'Malanak', 'Vasylovych', '1997', 'Lviv, Saharova str 5', 'link', 'link', '19', 'Yes', 2);
insert into student values(3, 'Petro', 'Halanar', 'Mykolaovych', '1997', 'Kyiv, Heroes str 8', 'link', 'link', '18', 'Yes', 3);
insert into student values(4, 'Andriy', 'Semenov', 'Mykolaovych', '1998', 'Ternopil, Zelena str 189-7', 'link', 'link', '17', 'Yes', 1);
insert into student values(5, 'Ivanna', 'Mykolaiva', 'Petrivna', '1997', 'Lutsk, Persha str 1-3', 'link', 'link', '16', 'Yes', 1);